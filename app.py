import json

class Result:
    market:float
    auction:float
    
    def _init_(self,auction,market):
        self.auction=auction
        self.market=market
        
def calculate(modelId:str,year:str)->Result:
    print(f"Year: {year}, Model: {modelId}")
    f = open ('api-response.json', "r")
    data = json.loads(f.read())
    f.close()
    
    equipmentFounded = None
    for equipment in data:

        if data[equipment]['classification']['model'] == modelId:
            equipmentFounded = data[equipment]
            break
    if equipmentFounded is None:
        raise Exception(f"The model {modelId} does not exist")
    if year not in equipmentFounded['schedule']['years']:
        raise Exception(f"The year {year} does not exist")
    
    marketValue = equipmentFounded['saleDetails']['cost']* equipmentFounded['schedule']['years'][year]['marketRatio']
    auctionValue = equipmentFounded['saleDetails']['cost']* equipmentFounded['schedule']['years'][year]['auctionRatio']
    return Result(auctionValue,marketValue)
        
    
try:  
    value = calculate("D8T","2006")
    print(f"marketValue: {value.market}, auctionValue: {value.auction}")
except Exception as e:
    print(str(e))